#usercontextdata.py
#I should have named it usercontext.py, but I didn't because I'm a stook

#import queue

class SystemResponse:
    
    def __init__(self):
        self.success = True
        self.messageLog = [] #a list of strings

class UserContextData:
    
    def __init__(self, fn, sr = None, akp = None):
        self.__lastEvaluationResponse = sr
        self.__answeredKeyPairs = akp
        self.__function = fn
    
    #These property methods are the closest to const public attributes as you
    #can get in Python. They are still returning the references though, so they
    #still need to be actively protected.
    
    @property
    def lastEvaluationResponse(self):
        return self.__lastEbaluationResponse
        
    @property
    def answeredKeyePairs(self):
        return self.__answeredKeyPairs
        
    @property
    def function(self):
        return self.__function
        
class UserContextHolder:
    
    def __init__(self):
        self.__undoStack = []
        self.__redoStack = []
        
    def undo(self):
        '''Love it or hate it, you can't miss it: the glorious, infamous ctrl-z.'''
        #only do anything if there's something in the undo stack to undo
        if len(self.__undoStack) > 0:
            self.__redoStack.append(self.undoStack.pop())
            
    def redo(self):
        
        #only do anything if there's something in the redo stack to undo
        if len(self.__redoStack) > 0:
            self.__undoStack.append(self.redoStack.pop())
            
    def restart(self):
        '''clear all history, but preserve the first UserContextData'''
        
        if len(self.__undoStack) > 0: # it would be really hard for this not to be true.
            firstContext = self.__undoStack[0]
            self.__undoStack.clear()
            self.__redoStack.clear()
            self.__undoStack.append(firstContext)
        else:
            #if len(self.__undoStack) <= 0, no need to clear it!
            self.__redoStack.clear()
    
    def getSolicitation(self):
        '''Gets the solicitation for the first argument in the DilaogFunction from the
most recent UserContextData (UserContextData on the back of the undo stack).'''
        pass
        
    def ProcessUserUtterance(self, utterance):
        '''Do all kinds of crazy stuff.'''
        pass
