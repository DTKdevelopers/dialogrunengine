#dialogmodel.py

class DialogPattern:
    
    dbInterface = None
    
    def evaluate(self):
        pass
    
class DialogExpression:
    
    def __init__(self):
        self.__patterns = []
        
    def evaluate(self):
        pass

class DialogFunction:
    
    def evaluate(userContext, utteranceKeyPairs):
        
        if not isinstance(userContext, UserContextData):
            raise TypeError('userContext must be UserContextData')
        if not isinstance(utteranceKeyPairs, dict):
            raise TypeError('utteranceKeyPairs must be dict')
